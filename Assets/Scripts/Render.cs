using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Recorder;
using UnityEditor.Recorder.Input;
#endif
using UnityEngine;
using UnityEngine.Playables;
public class Render : MonoBehaviour
{
#if UNITY_EDITOR
    RecorderController m_RecorderController;
#endif
    public PlayableDirector timeLine;
    public ModelHero modelHero;
    public int config = 0;
    public List<Heros> heros = new List<Heros>();

    RecorderControllerSettings controllerSettings;
    MovieRecorderSettings videoRecorder;
    string mediaOutputFolder;


    public void SettingRecoder()
    {
        controllerSettings = ScriptableObject.CreateInstance<RecorderControllerSettings>();
        m_RecorderController = new RecorderController(controllerSettings);
        mediaOutputFolder = Path.Combine(Application.dataPath, "..", "SampleRecordings");

        videoRecorder = ScriptableObject.CreateInstance<MovieRecorderSettings>();
        videoRecorder.Enabled = true;
        videoRecorder.OutputFormat = MovieRecorderSettings.VideoRecorderOutputFormat.MP4;
        videoRecorder.VideoBitRateMode = VideoBitrateMode.High;
        videoRecorder.ImageInputSettings = new GameViewInputSettings
        {
            OutputWidth = 1920,
            OutputHeight = 1080
        };

        controllerSettings.SetRecordModeToFrameInterval(0, 240);
        controllerSettings.FrameRate = 30;

    }
    public void SetBodyPart(Heros heros)
    {
        BodyPart[] bodyParts = new BodyPart[5];
        int numberRare = Random.Range(0, 3);
        int numberRace = RandomRace();
        int numberClass= Random.Range(1, 6);
        for (int i = 0; i < numberRare; i++)
        {
            bodyParts[i] = new BodyPart();
            SetRarityRacePart(bodyParts[i], Rarity.rare);
        }
        for (int i = numberRare; i < 5; i++)
        {
            bodyParts[i] = new BodyPart();
            SetRarityRacePart(bodyParts[i], Rarity.common);
        }
        IListExtensions.Shuffle(bodyParts);
        heros.HeadId = bodyParts[0];
        heros.TopId = bodyParts[1];
        heros.LegId = bodyParts[2];
        heros.WingId = bodyParts[3];
        heros.EyeId = bodyParts[4];
        heros.Class = numberClass;
        heros.Race = numberRace;
    }


    IEnumerator RenderHeros()
    {
        SettingRecoder();
        for (int i = 0; i < 7000; i++)
        {
            Heros hero = new Heros();
            SetBodyPart(hero);
            for (int j = 0; j < heros.Count; j++)
            {
                bool check = CheckConfigHero(hero, heros[j]);
                if (check)
                {
                    SetBodyPart(hero);
                    //Debug.LogError(i + " : " + j);
                    j = 0;
                }
            }
            heros.Add(hero);
            CreateNewHero(hero);
            videoRecorder.OutputFile = Path.Combine(mediaOutputFolder, "video_") + i.ToString();
            controllerSettings.AddRecorderSettings(videoRecorder);
            m_RecorderController.PrepareRecording();
            m_RecorderController.StartRecording();
            yield return new WaitForSeconds(8.5f);
            m_RecorderController.StopRecording();
        }
    }
    public bool CheckConfigHero(Heros hero, Heros hero1)
    {
        bool head = CheckConfigBodyPart(hero.HeadId, hero1.HeadId);
        bool top = CheckConfigBodyPart(hero.TopId, hero1.TopId);
        bool leg = CheckConfigBodyPart(hero.LegId, hero1.LegId);
        bool wing = CheckConfigBodyPart(hero.WingId, hero1.WingId);
        bool eye = CheckConfigBodyPart(hero.EyeId, hero1.EyeId);
        return (head && top && leg && wing && eye);
    }    public bool CheckConfigBodyPart(BodyPart part, BodyPart part1)

    {
        return (part.Rarity == part1.Rarity && part.Race == part1.Race);
    }
    public void SetRarityRacePart(BodyPart bodyPart, Rarity maxRarityAvarilable)
    {
        bodyPart.Rarity = maxRarityAvarilable;
        bodyPart.Race = RandomRace();
    }
    public int RandomRace()
    {
        return Random.Range(1, 3);
    }
    public void CreateNewHero(Heros hero)
    {
        ResetModel();
        int body = hero.Race;
        int head = 4 * (hero.HeadId.Race - 1) + (int)(hero.HeadId.Rarity);
        int top = 4 * (hero.TopId.Race - 1) + (int)(hero.TopId.Rarity);
        int leg = 4 * (hero.LegId.Race - 1) + (int)(hero.LegId.Rarity);
        int wing = 4 * (hero.WingId.Race - 1) + (int)(hero.WingId.Rarity);
        int eye = 4 * (hero.EyeId.Race - 1) + (int)(hero.EyeId.Rarity);

        modelHero.BodyHero.transform.GetChild(body - 1).gameObject.SetActive(true);
        modelHero.Heads.transform.GetChild(head).gameObject.SetActive(true);
        modelHero.Tops.transform.GetChild(top).gameObject.SetActive(true);
        modelHero.Legs.transform.GetChild(leg).gameObject.SetActive(true);
        modelHero.Wings.transform.GetChild(wing).gameObject.SetActive(true);
        modelHero.Eyes.transform.GetChild(eye).gameObject.SetActive(true);
    }
    public void ResetModel()
    {
        for(int i = 0; i < modelHero.Eyes.transform.childCount; i++)
        {
            modelHero.Eyes.transform.GetChild(i).gameObject.SetActive(false);
            modelHero.Heads.transform.GetChild(i).gameObject.SetActive(false);
            modelHero.Legs.transform.GetChild(i).gameObject.SetActive(false);
            modelHero.Wings.transform.GetChild(i).gameObject.SetActive(false);
            modelHero.Tops.transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    //IEnumerator RenderVideo()
    //{
    //    SettingRecoder();

    //    for (int i = 0; i < 30; i++)
    //    {
    //        videoRecorder.OutputFile = Path.Combine(mediaOutputFolder, "video_") + i.ToString();
    //        controllerSettings.AddRecorderSettings(videoRecorder);
    //        controllerSettings.SetRecordModeToFrameInterval(0, 240);
    //        controllerSettings.FrameRate = 30;
    //        m_RecorderController.PrepareRecording();
    //        m_RecorderController.StartRecording();
    //        yield return new WaitForSeconds(8.5f);
    //        ResetModel();
    //        timeLine.Stop();
    //        m_RecorderController.StopRecording();
    //        timeLine.time = 0;
    //        timeLine.Play();
    //    }


    //    m_RecorderController.StopRecording();
    //    EditorApplication.isPlaying = false;
    //}
    private void Start()
    {
        StartCoroutine(RenderHeros());
    }
}
[System.Serializable]
public static class IListExtensions
{
    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        for (var i = 0; i < count; i++)
        {
            var r = Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
}
[System.Serializable]
public class ModelHero
{
    public GameObject BodyHero, Heads, Tops, Legs, Wings, Eyes;
}