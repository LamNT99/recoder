using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Heros
{
    public BodyPart HeadId;
    public BodyPart TopId;
    public BodyPart LegId;
    public BodyPart WingId;
    public BodyPart EyeId;

    public int Class;
    public int Race;
}
[System.Serializable]
public class BodyPart
{
    public Rarity Rarity;
    public int Race;
}
[System.Serializable]
public enum Rarity
{
    common,
    rare,
    epic,
    legend
}